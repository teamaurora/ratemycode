var editor;

function setupEditableEditor() {
    editor = ace.edit("code");
    editor.setTheme("ace/theme/crimson");
    editor.getSession().setMode("ace/mode/text");
    function onPost() {
        var textarea = $('textarea[name="code"]').hide();
        textarea.val(editor.getSession().getValue());
    }
}

function setupReadOnlyEditor(language) {
    editor = ace.edit("code");
    editor.setTheme("ace/theme/crimson");
    editor.getSession().setMode("ace/mode/" + language);
}

function onEditorPost() {
    var textarea = $('textarea[name="code"]').hide();
    textarea.val(editor.getSession().getValue());

    console.log(textarea.val());
}