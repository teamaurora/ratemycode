function setupVoteButtons() {
    var btns = ['upvote', 'downvote'];
    var input = document.getElementById('vote_type');
    for (var i = 0; i < btns.length; i++) {
        document.getElementById(btns[i]).addEventListener('click', function () {
            input.value = this.value;
        });
    }
}