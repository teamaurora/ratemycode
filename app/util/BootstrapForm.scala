package util

import views.html.bootstrap.bootstrapForm
import views.html.helper.FieldConstructor

/**
 * @author rvbiljouw
 */
object BootstrapForm {

  implicit val myFields = FieldConstructor(bootstrapForm.f)

}
