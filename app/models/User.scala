package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._
import java.security.MessageDigest

/**
 * @author rvbiljouw
 */
case class User(id: Pk[Long], displayName: String, emailAddress: String, password: String, verified: Boolean)

object User {

  val simple = {
    get[Pk[Long]]("id") ~
      get[String]("display_name") ~
      get[String]("email_address") ~
      get[String]("password") ~
      get[Boolean]("verified") map {
      case id ~ displayName ~ emailAddress ~ password ~ verified => User(id, displayName, emailAddress, password, verified)
    }
  }

  def create(displayName: String, emailAddress: String, password: String): User = {
    DB.withConnection(
      implicit conn =>
        SQL(
          """
          insert into user (display_name, email_address, password, verified)
          values({displayName}, {emailAddress}, {password}, 0)
          """).on(
          'displayName -> displayName,
          'emailAddress -> emailAddress,
          'password -> md5(password)
        ).executeInsert()
    ) match {
      case None => null
      case Some(key) => findById(key.asInstanceOf[Long])
    }
  }

  def findById(id: Long): User = DB.withConnection {
    implicit conn =>
      SQL("select * from user where id = {id}").on('id -> id).as(simple.single)
  }

  def findByDetails(emailAddress: String, password: String): User = DB.withConnection {
    implicit conn =>
      SQL(
        """
          select * from user
          where email_address = {emailAddress} and password = {password}
        """).on('emailAddress -> emailAddress, 'password -> md5(password)).as(simple.singleOpt).getOrElse(null)
  }

  def findByEmail(emailAddress: String): User = DB.withConnection {
    implicit conn =>
      SQL(
        """
          select * from user
          where email_address = {emailAddress}
        """).on('emailAddress -> emailAddress).as(simple.singleOpt).getOrElse(null)
  }

  def findByDisplayName(displayName: String): List[User] = DB.withConnection {
    implicit conn =>
      SQL(
        """
          select * from user
          where display_name = {displayName}
        """).on('displayName -> displayName).as(simple *)
  }


  def md5(string: String): String = {
    MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8")).
      map("%02X".format(_)).mkString
  }
}
