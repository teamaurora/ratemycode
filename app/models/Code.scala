package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._

/**
 * @author tobiewarburton
 */
case class Code(id: Pk[Long], parentId: Long, name: String, description: String, code: String)

object Code {

  val simple = {
    get[Pk[Long]]("id") ~
      get[Long]("parent_id") ~
      get[String]("name") ~
      get[String]("description") ~
      get[String]("code") map {
      case id ~ parentId ~ name ~ description ~ code => Code(id, parentId, name, description, code)
    }
  }

  def create(parentId: Long, name: String, description: String, code: String): Code = {
    DB.withConnection(
      implicit conn =>
        SQL(
          """
          insert into code (parent_id, name, description, code)
          values({parentId}, {name}, {description}, {code})
          """).on(
          'parentId -> parentId,
          'name -> name,
          'description -> description,
          'code -> code
        ).executeInsert()
    ) match {
      case None => null
      case Some(key) => findById(key.asInstanceOf[Long])
    }
  }

  /**
   * probably not needed, added so possibly add fullscreen viewing of single classfile? or for rendering plain text
   */
  def findById(id: Long): Code = DB.withConnection {
    implicit conn =>
      SQL("select * from code where id = {id}").on('id -> id).as(simple.single)
  }

  def findByParent(parentId: Long): List[Code] = DB.withConnection {
    implicit conn =>
      SQL(
        """
          select * from code
          where parent_id = {submissionId}
        """).on('submissionId -> parentId).as(simple *)
  }
}
