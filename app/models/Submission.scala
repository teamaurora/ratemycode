package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._
import java.util.Date

/**
 * A case class representing a Submission
 * @author rvbiljouw
 */
case class Submission(id: Pk[Long], parent_id: Long, language: Long,
                      name: String, description: String, positive: Long, negative: Long,
                      date: Date, published: Boolean)

object Submission {

  val simple = {
    get[Pk[Long]]("submission.id") ~
      get[Long]("submission.parent_id") ~
      get[Long]("submission.language") ~
      get[String]("submission.name") ~
      get[String]("submission.description") ~
      get[Long]("positive") ~
      get[Long]("negative") ~
      get[Date]("submission.date") ~
      get[Boolean]("submission.published") map {
      case id ~ parent_id ~ language ~ name ~ description ~ positive ~ negative ~ date ~ published =>
        Submission(id, parent_id, language, name, description, positive, negative, date, published)
    }
  }

  def create(parent_id: Long, language: Long, name: String, description: String, published: Boolean): (Submission, Language) = {
    DB.withConnection(
      implicit conn =>
        SQL(
          """
            insert into submission (parent_id, language, name, description, published, date)
            values(
              {parent_id},
              {language},
              {name},
              {description},
              {published},
              NOW()
            )
          """
        ).on(
          'parent_id -> parent_id,
          'language -> language,
          'name -> name,
          'description -> description,
          'published -> published
        ).executeInsert()
    ) match {
      case Some(key) => findById(key.asInstanceOf[Long])
      case None => null
    }
  }

  def setPublished(id: Long, published: Boolean): (Submission, Language) = {
    DB.withConnection(
      implicit conn =>
        SQL(
          """
      update submission set published = {published} where id = {id}
          """
        ).on(
          'published -> published,
          'id -> id
        ).executeInsert()
    ) match {
      case Some(key) => findById(key.asInstanceOf[Long])
      case None => null
    }
  }

  def delete(id: Long) {
    DB.withConnection {
      implicit conn =>
        SQL("delete from submission where submission.id = {id}").
          on('id -> id)
    }
  }

  def findCount: Long = DB.withConnection {
    implicit conn =>
      SQL("select count(*) from submission where submission.published = 1").as(scalar[Long].single)
  }

  def findCountByQuery(query: String): Long = DB.withConnection {
    implicit conn =>
      SQL(
        """select count(*), submission.*, language.* from submission
           join language on submission.language = language.id
           where (submission.name LIKE {query}
             OR submission.description LIKE {query}
             OR language.name LIKE {query})
             AND submission.published = 1
        """).on('query -> ("%" + query + "%")).as(scalar[Long].single)
  }


  def findCountByQueryAlt(query: String): Long = DB.withConnection {
    implicit conn =>
      SQL(
        """select count(*), submission.*, language.* from submission
           join language on submission.language = language.id
           where """ + query + """ 1 = 1 AND submission.published = 1
                               """).as(scalar[Long].single)
  }


  def findAll: List[(Submission, Language)] = DB.withConnection {
    implicit conn =>
      SQL(
        """
          select language.*, submission.*,
            (select COUNT(*) from vote positive
             where positive.submission = submission.id
             and positive.positive = 1) as positive,
             (select COUNT(*) from vote negative
             where negative.submission = submission.id
             and negative.positive = 0) as negative
          from submission
          join language on submission.language = language.id
          order by submission.date desc
          where submission.published = 1
        """
      ).
        as((Submission.simple ~ Language.simple map {
        case submission ~ language => submission -> language
      }) *)
  }

  def findTrending(page: Int, parentId: Long): List[(Submission, Language)] = DB.withConnection {
    implicit conn =>
      SQL(
        """
          select language.*, submission.*,
            (select COUNT(*) from vote positive
             where positive.submission = submission.id
             and positive.positive = 1) as positive,
             (select COUNT(*) from vote negative
             where negative.submission = submission.id
             and negative.positive = 0) as negative
          from submission
          join language on submission.language = language.id
          where submission.published = 1 %s
          order by (positive - negative) desc
          limit 25 offset {offset}
        """.format(if (parentId == -1) "" else (" and submission.parent_id = " + parentId))
      ).on(
        'offset -> (page * 25)
      ).
        as((Submission.simple ~ Language.simple map {
        case submission ~ language => submission -> language
      }) *)
  }

  def findTrending(page: Int): List[(Submission, Language)] = findTrending(page, -1)

  def findRecent(page: Int, parentId: Long): List[(Submission, Language)] = DB.withConnection {
    implicit conn =>
      SQL(
        """
          select language.*, submission.*,
            (select COUNT(*) from vote positive
             where positive.submission = submission.id
             and positive.positive = 1) as positive,
             (select COUNT(*) from vote negative
             where negative.submission = submission.id
             and negative.positive = 0) as negative
          from submission
          join language on submission.language = language.id
          where submission.published = 1 %s
          order by submission.date desc
          limit 25 offset {offset}
        """.format(if (parentId == -1) "" else (" and submission.parent_id = " + parentId))
      ).on(
        'offset -> (page * 25)
      ).
        as((Submission.simple ~ Language.simple map {
        case submission ~ language => submission -> language
      }) *)
  }

  def findRecent(page: Int): List[(Submission, Language)] = findRecent(page, -1)

  def findByQuery(query: String, page: Int): List[(Submission, Language)] = DB.withConnection {
    implicit conn =>
      SQL(
        """
          select language.*, submission.*,
            (select COUNT(*) from vote positive
            where positive.submission = submission.id
            and positive.positive = 1) as positive,
            (select COUNT(*) from vote negative
            where negative.submission = submission.id
            and negative.positive = 0) as negative
          from submission
          join language on submission.language = language.id
          where (submission.name LIKE {query}
            OR submission.description LIKE {query}
            OR language.name LIKE {query}) and submission.published = 1
        """).on('query -> ("%" + query + "%"), 'offset -> (page * 25)).as(
        (Submission.simple ~ Language.simple map {
          case submission ~ language => submission -> language
        }) *
      )
  }

  def findByQueryAlt(query: String, page: Int): List[(Submission, Language)] = DB.withConnection {
    implicit conn =>
      SQL(
        """
          select language.*, submission.*,
            (select COUNT(*) from vote positive
            where positive.submission = submission.id
            and positive.positive = 1) as positive,
            (select COUNT(*) from vote negative
            where negative.submission = submission.id
            and negative.positive = 0) as negative
          from submission
          join language on submission.language = language.id
          where """ + query + " 1 = 1 and submission.published = 1").on('offset -> (page * 25)).as(
        (Submission.simple ~ Language.simple map {
          case submission ~ language => submission -> language
        }) *
      )
  }

  def findByLanguage(languageId: Long): List[(Submission, Language)] = DB.withConnection {
    implicit conn =>
      SQL(
        """
          select language.*, submission.*,
            (select COUNT(*) from vote positive
            where positive.submission = submission.id
            and positive.positive = 1) as positive,
            (select COUNT(*) from vote negative
            where negative.submission = submission.id
            and negative.positive = 0) as negative
          from submission
          join language on submission.language = language.id
          where submission.language = {languageId}
          and submission.published = 1
        """
      ).on('languageId -> languageId).as(
        (Submission.simple ~ Language.simple map {
          case submission ~ language => submission -> language
        }) *
      )
  }

  def findById(id: Long): (Submission, Language) = DB.withConnection {
    implicit conn =>
      SQL(
        """
           select language.*, submission.*,
            (select COUNT(*) from vote positive
                      where positive.submission = submission.id
                      and positive.positive = 1) as positive,
                      (select COUNT(*) from vote negative
                      where negative.submission = submission.id
                      and negative.positive = 0) as negative
                      from submission
            join language on submission.language = language.id
            where submission.id = {id}
            order by submission.date desc
        """)
        .on('id -> id).as((Submission.simple ~ Language.simple map {
        case submission ~ language => submission -> language
      }).single)
  }

  def findByParent(parent_id: Long): List[(Submission, Language)] = DB.withConnection {
    implicit conn =>
      SQL(
        """
           select language.*, submission.*,
            (select COUNT(*) from vote positive
                      where positive.submission = submission.id
                      and positive.positive = 1) as positive,
                      (select COUNT(*) from vote negative
                      where negative.submission = submission.id
                      and negative.positive = 0) as negative
                      from submission
            join language on submission.language = language.id
            where submission.parent_id = {parent_id}
            order by submission.date desc
        """)
        .on('parent_id -> parent_id).as((Submission.simple ~ Language.simple map {
        case submission ~ language => submission -> language
      }) *)
  }
}
