package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._

/**
 * A case class being our language object.
 * @author rvbiljouw
 */
case class Language(id: Pk[Long], name: String, mode: String)

object Language {

  /**
   * A mapper that creates an object of type Language from a SQL result set.
   */
  val simple = {
    get[Pk[Long]]("language.id") ~
      get[String]("language.name") ~
      get[String]("language.mode") map {
      case id ~ name ~ mode => Language(id, name, mode)
    }
  }

  /**
   * Inserts a language into the database
   * @param name Name of the language
   * @param mode Name of the highlighting mode in AceEditor
   */
  def create(name: String, mode: String): Language = {
    DB.withConnection {
      implicit conn => {
        SQL(
          """
            insert into language (name, mode) values({name},{mode})
          """
        ).on('name -> name, 'mode -> mode).executeInsert()
      } match {
        case Some(key) => findById(key.asInstanceOf[Long])
        case None => null
      }
    }
  }

  /**
   * Deletes a language from the database
   * @param id ID of the language to delete
   */
  def delete(id: Long) {
    DB.withConnection {
      implicit conn => {
        SQL("delete from language where id = {id}").
          on('id -> id).executeUpdate()
      }
    }
  }

  /**
   * Finds all languages in the database and returns them in a list
   * @return list of languages
   */
  def findAll(): List[Language] = DB.withConnection {
    implicit conn =>
      SQL("select * from language").as(simple *)
  }

  /**
   * Finds a language by it's id and returns it
   * @param id ID of the language
   * @return language
   */
  def findById(id: Long): Language = DB.withConnection {
    implicit conn =>
      SQL("select * from language where id = {id}").
        on('id -> id).as(simple.single)
  }

  /**
   * Finds a language by its name
   * @param name language name
   * @return language
   */
  def findByName(name: String): Language = DB.withConnection {
    implicit conn =>
      SQL("select * from language where LOWER(name) = {name}").
        on('name -> name.toLowerCase).as(simple.single)
  }
}