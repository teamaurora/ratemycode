package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._
import java.util.Date

/**
 * @author rvbiljouw
 */
case class Comment(id: Pk[Long], parent: Long, submission: Long,
                   username: String, subject: String, comment: String, date: Date)

object Comment {

  val simple = {
    get[Pk[Long]]("comment.id") ~
      get[Long]("comment.parent") ~
      get[Long]("comment.submission") ~
      get[String]("comment.username") ~
      get[String]("comment.subject") ~
      get[String]("comment.comment") ~
      get[Date]("comment.date") map {
      case id ~ parent ~ submission ~ username ~ subject ~ comment ~ date =>
        Comment(id, parent, submission, username, subject, comment, date)
    }
  }

  def create(submission: Long, parent: Long, username: String, subject: String, comment: String) = {
    DB.withConnection {
      implicit conn =>
        SQL(
          """
          insert into comment (submission, parent, username, subject, comment, date)
          values ({parent}, {submission}, {username}, {subject}, {comment}, NOW())
          """).on(
          'submission -> submission,
          'parent -> parent,
          'username -> username,
          'subject -> subject,
          'comment -> comment
        ).executeInsert()
    } match {
      case None => null
      case Some(key) => findById(key.asInstanceOf[Long])
    }
  }

  def findById(id: Long) = {
    DB.withConnection(
      implicit conn =>
        SQL("select * from comment where id = {id}").
          on('id -> id).as(simple.single)
    )
  }

  def findBySubmission(submission: Long): List[Comment] = {
    DB.withConnection(
      implicit conn =>
        SQL("select * from comment where submission = {submission} and parent = 0").
          on('submission -> submission).as(simple *)
    )
  }

  def findByComment(comment: Long): List[Comment] = {
    DB.withConnection(
      implicit conn =>
        SQL("select * from comment where parent = {parent}").
          on('parent -> comment).as(simple *)
    )
  }
}
