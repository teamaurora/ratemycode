package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._
import java.util.Date

/**
 * @author rvbiljouw
 */
case class Vote(id: Pk[Long], submission: Long, positive: Boolean, hostName: String, date: Date)

object Vote {

  val simple = {
    get[Pk[Long]]("vote.id") ~
      get[Long]("vote.submission") ~
      get[Int]("vote.positive") ~
      get[String]("vote.hostname") ~
      get[Date]("vote.date") map {
      case id ~ submission ~ positive ~ hostName ~ date =>
        Vote(id, submission, positive == 1, hostName, date)
    }
  }

  def create(submission: Long, positive: Boolean, hostName: String) = {
    DB.withConnection(implicit conn => {
      SQL(
        """
          insert into vote (submission, positive, hostname, date)
          values ({submission}, {positive}, {hostName}, NOW())
        """).on('submission -> submission, 'positive -> positive, 'hostName -> hostName).executeInsert()
    })
  }

  def findAll(submissionId: Long): List[Vote] = {
    DB.withConnection(
      implicit conn =>
        SQL("select * from vote where submission = {submissionId}")
          .on('submissionId -> submissionId).as(simple *)
    )
  }

  def findPositive(submissionId: Long): List[Vote] = {
    DB.withConnection {
      implicit conn =>
        SQL("select * from vote where submission = {submissionId} and positive = 1")
          .on('submissionId -> submissionId).as(simple *)
    }
  }

  def findNegative(submissionId: Long): List[Vote] = {
    DB.withConnection {
      implicit conn =>
        SQL("select * from vote where submission = {submissionId} and positive = 0")
          .on('submissionId -> submissionId).as(simple *)
    }
  }

  def findByHost(submissionId: Long, hostName: String): List[Vote] = {
    DB.withConnection {
      implicit conn =>
        SQL("select * from vote where submission = {submission} and hostname = {hostname}")
          .on('submission -> submissionId, 'hostname -> hostName).as(simple *)
    }
  }
}
