package controllers

import play.api.mvc._
import views.html.{recent_list, trending_list, home}
import models.Submission

object Application extends Controller {

  def index = Action {
    val trending = Submission.findTrending(0)
    val recent = Submission.findRecent(0)
    Ok(home.render(trending, recent))
  }

  def trending(page: Int) = Action {
    val trending = Submission.findTrending(page)
    val count = (Submission.findCount / 25).toInt
    Ok(trending_list.render(trending, page, count))
  }

  def recent(page: Int) = Action {
    val recent = Submission.findRecent(page)
    val count = (Submission.findCount / 25).toInt
    Ok(recent_list.render(recent, page, count))
  }

}