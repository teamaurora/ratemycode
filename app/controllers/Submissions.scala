package controllers

import play.api.data.Forms._
import play.api.data.Form
import play.api.mvc._
import views.html.{add_code, search_results, view_submission, submit_form}
import models._
import play.api.cache.Cache
import play.api.Play.current

/**
 * @author tobiewarburton
 * @author rvbiljouw
 */
object Submissions extends Controller {

  val voteForm = Form(tuple(
    "submission" -> nonEmptyText,
    "vote_type" -> nonEmptyText
  ))

  val submitForm = Form(tuple(
    "language" -> nonEmptyText,
    "name" -> nonEmptyText,
    "description" -> nonEmptyText
  ))

  val codeForm = Form(tuple(
    "publish" -> nonEmptyText,
    "name" -> nonEmptyText,
    "description" -> nonEmptyText,
    "code" -> nonEmptyText
  ))

  val commentForm = Form(tuple(
    "parent" -> nonEmptyText,
    "submission" -> nonEmptyText,
    "username" -> nonEmptyText,
    "subject" -> nonEmptyText,
    "comment" -> nonEmptyText
  ))

  val searchForm = Form(
    "query" -> nonEmptyText
  )

  def submit = Action {
    Ok(submit_form.render(submitForm))
  }

  def handleSubmission = Action {
    implicit request =>
      val filledSubmitForm = submitForm.bindFromRequest()
      filledSubmitForm.fold(
        errors => {
          BadRequest(submit_form.render(filledSubmitForm))
        },
        formContent => {
          val lang = formContent._1.toLong
          val name = formContent._2
          val desc = formContent._3

          val key = session.get("user").getOrElse("undef")
          val user = Cache.get(key).getOrElse(null).asInstanceOf[User]
          val pid: Long = if (user == null) -1 else user.id.get
          val submission = Submission.create(pid, lang, name, desc, false)
          if (submission._1 != null) {
            Cache.set(request.host, submission._1)
            Redirect(routes.Submissions.code())
          } else {
            BadRequest("Oops, something went wrong here!")
          }
        }
      )
  }

  def code = Action {
    implicit request => {
      val submission = Cache.get(request.host).get
      if(submission != null) {
        Ok(add_code.render(codeForm))
      } else {
        Redirect(routes.Submissions.submit())
      }
    }
  }

  def handleCode = Action {
    implicit request =>
      val filledCodeForm = codeForm.bindFromRequest()
      filledCodeForm.fold(
        errors => {
          BadRequest(add_code.render(filledCodeForm))
        },
        formContent => {
          val publish = formContent._1
          val name = formContent._2
          val desc = formContent._3
          val data = formContent._4

          val currentSubmission = Cache.getAs[Submission](request.host).get
          if (currentSubmission != null) {
            Code.create(currentSubmission.id.get, name, desc, data)
            if (publish.equals("1")) {
              Submission.setPublished(currentSubmission.id.get, published = true)
              Cache.set(request.host, null)
              Redirect(routes.Submissions.view(currentSubmission.id.get))
            } else {
              Ok(add_code.render(codeForm))
            }
          } else {
            BadRequest("wat")
          }
        }
      )
  }

  def handleVote = Action {
    implicit request =>
      val filledVoteForm = voteForm.bindFromRequest()
      filledVoteForm.fold(
        errors => {
          BadRequest("u fukd up bro")
        },
        formContent => {
          val submission = Submission.findById(formContent._1.toLong)
          val submissionId = submission._1.id.get
          val voteType = formContent._2.toBoolean
          Vote.create(submissionId, voteType, request.host)
          Redirect(routes.Submissions.view(submissionId))
        }
      )
  }

  def handleComment = Action {
    implicit request =>
      val filledCommentForm = commentForm.bindFromRequest()
      filledCommentForm.fold(
        errors => {
          BadRequest("derp derp")
        },
        formContent => {
          val parentId = formContent._1.toLong
          val submissionId = formContent._2.toLong
          val comment = Comment.create(parentId, submissionId, formContent._3, formContent._4, formContent._5)

          Redirect(routes.Submissions.view(submissionId))
        }
      )
  }

  def handleSearch(page: Int) = Action {
    implicit request =>
      val filledSearchForm = searchForm.bindFromRequest()
      filledSearchForm.fold(
        errors => {
          Redirect(routes.Application.index())
        },
        formContent => {
          val searchQuery = formulateSearchQuery(formContent)
          val result_count = Submission.findCountByQueryAlt(searchQuery)
          val submissions = Submission.findByQueryAlt(searchQuery, page)
          Ok(search_results.render(submissions, formContent, page, (result_count / 25).toInt))
        }
      )
  }

  def formulateSearchQuery(query: String): String = {
    val pieces = query.split(" ")
    val baseQuery =
      """
        (submission.name LIKE {query}
                    OR submission.description LIKE {query}
                    OR language.name LIKE {query})
      """
    var multiQuery = ""

    for (piece <- pieces) {
      multiQuery += baseQuery.replaceAll("\\{query\\}", "'%" + piece + "%'") + " and" // Do some SQL escaping later..
    }
    multiQuery
  }

  def view(id: Long, code: Int) = Action {
    implicit request => {
      val submission = Submission.findById(id)
      val codes = Code.findByParent(id)
      val votes = Vote.findByHost(id, request.host)
      if (votes.size == 0) {
        Ok(view_submission.render(submission, codes, code, null))
      } else {
        Ok(view_submission.render(submission, codes, code,
          "You have already voted."))
      }
    }
  }

}
