package controllers

import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc._
import views.html.{user_index, login_form, register_form}
import models.User
import play.api.cache.Cache
import play.api.Play.current

/**
 * @author rvbiljouw
 */
object Users extends Controller {

  val loginForm = Form(
    tuple(
      "emailAddress" -> nonEmptyText,
      "password" -> nonEmptyText
    )
      verifying("Invalid username/password", user =>
      User.findByDetails(user._1, user._2) != null
      )
  )

  val registerForm = Form(
    tuple(
      "emailAddress" -> nonEmptyText.verifying("E-mail address already registered",
        emailAddress => User.findByEmail(emailAddress) == null),
      "displayName" -> nonEmptyText.verifying("Display name already taken",
        displayName => User.findByDisplayName(displayName).size == 0),
      "password" -> nonEmptyText,
      "passwordRepeat" -> nonEmptyText
    )
  )

  def index = Action {
    implicit request => {
      val key = session.get("user").getOrElse("undef")
      val user = User.findByEmail(key)
      if (user != null) {
        Ok(user_index.render(user))
      } else {
        Redirect(routes.Users.login())
      }
    }
  }

  def login = Action {
    Ok(login_form.render(loginForm))
  }

  def handleLogin = Action {
    implicit request => {
      val filledForm = loginForm.bindFromRequest()
      filledForm.fold(
        hasErrors => {
          BadRequest(login_form.render(filledForm))
        },
        formContent => {
          val user = User.findByDetails(formContent._1, formContent._2)
          if (user != null) {
            Redirect(routes.Users.index).withSession(session + ("user" -> user.emailAddress))
          } else {
            BadRequest("Oops!")
          }
        }
      )
    }
  }

  def register = Action {
    Ok(register_form.render(registerForm))
  }

  def handleRegister = Action {
    implicit request => {
      val filledForm = registerForm.bindFromRequest()
      filledForm.fold(
        hasErrors => {
          BadRequest(register_form.render(filledForm))
        },
        formContent => {
          val user = User.create(formContent._2, formContent._1, formContent._3)
          if (user != null) {
            Redirect(routes.Application.index())
          } else {
            BadRequest("Failed to insert user...")
          }
        }
      )
    }
  }
}
