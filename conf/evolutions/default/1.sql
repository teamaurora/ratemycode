# RMC Schema

# --- !Ups
CREATE TABLE `comment` (
  `id`         INT(255) NOT NULL AUTO_INCREMENT,
  `parent`     INT(255) DEFAULT NULL,
  `submission` INT(255) DEFAULT NULL,
  `username`   VARCHAR(40) DEFAULT NULL,
  `subject`    VARCHAR(40) DEFAULT NULL,
  `comment`    VARCHAR(2000) DEFAULT NULL,
  `date`       DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `language` (
  `id`   INT(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45)       NOT NULL,
  `mode` VARCHAR(45)       NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `mode_UNIQUE` (`mode`)
);

CREATE TABLE `submission` (
  `id`          INT(255)     NOT NULL AUTO_INCREMENT,
  `parent_id`   INT(255)     NOT NULL,
  `language`    INT(255)     NOT NULL,
  `name`        VARCHAR(45)  NOT NULL,
  `description` VARCHAR(200) NOT NULL,
  `published`   TINYINT(1),
  `date`        DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `code` (
  `id`          INT(255)       NOT NULL AUTO_INCREMENT,
  `parent_id`   INT(255)       NOT NULL,
  `name`        VARCHAR(45)    NOT NULL,
  `description` VARCHAR(200)   NOT NULL,
  `code`        VARCHAR(20000) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `vote` (
  `id`         INT(255)    NOT NULL AUTO_INCREMENT,
  `submission` INT(255)    NOT NULL,
  `positive`   TINYINT(4)  NOT NULL,
  `hostname`   VARCHAR(45) NOT NULL,
  `date`       DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `user` (
  `id`            INT(255)     NOT NULL AUTO_INCREMENT,
  `display_name`  VARCHAR(40)  NOT NULL,
  `email_address` VARCHAR(80)  NOT NULL,
  `password`      VARCHAR(100) NOT NULL,
  `verified`      TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `display_name_UNIQUE` (`display_name`),
  UNIQUE KEY `email_address_UNIQUE` (`email_address`)
);

INSERT INTO `language` (`name`, `mode`) VALUES ('Java', 'java');
INSERT INTO `language` (`name`, `mode`) VALUES ('Scala', 'scala');

# --- !Downs
SET foreign_key_checks = 0;
DROP TABLE `language`;
DROP TABLE `submission`;
DROP TABLE `vote`;
DROP TABLE `comment`;
DROP TABLE `user`;

